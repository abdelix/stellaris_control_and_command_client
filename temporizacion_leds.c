/*
 * temprizacion_leds.c
 *
 */
#include "FreeRTOS.h"
#include "timers.h"
#include "reloj.h"
#include "mi_log.h"
#include "drivers/rgb.h"
//#define NUM_TIMERS 5

/* An array to hold handles to the created timers. */
//TimerHandle_t xTimers[ NUM_TIMERS ];



void sw_timer_callback_enable( TimerHandle_t xTimer )
{
	     uint32_t g_ulColors[3] = { 0x0000, 0x0000, 0x0000 };


		// encender leds
		  g_ulColors[0]=0xFFFF;
		  g_ulColors[1]=0xFFFF;
		  g_ulColors[2]=0xFFFF;

		  RGBColorSet(g_ulColors);

		  // ya no es necesario el timer lo borramos si es possible
		  	xTimerDelete( xTimer,0 );
}
  void sw_timer_callback_disable( TimerHandle_t xTimer )
  {
	    uint32_t g_ulColors[3] = { 0x0000, 0x0000, 0x0000 };
		//apagar_leds
		g_ulColors[0]=0;
		g_ulColors[1]=0;
		g_ulColors[2]=0;

	  RGBColorSet(g_ulColors);

	// ya no es necesario el timer lo borramos si es possible
	xTimerDelete( xTimer,0 );

}

void  temporizar_leds(PARAM_COMANDO_PROGRAMAR_LEDS *param)
{
	TimerHandle_t xTimer;

    int ticks_count=(param->time-get_time());
    ticks_count*=configTICK_RATE_HZ;



    if(ticks_count>0)
    {
    	// Creamos el timer con id :0-> apagar � 1-> encender :

    	if(param->enable)
    	{
    		xTimer=xTimerCreate("Timer_Leds",ticks_count,pdFALSE,&param->enable,sw_timer_callback_enable);
    	} else{
    		xTimer=xTimerCreate("Timer_Leds",ticks_count,pdFALSE,&param->enable,sw_timer_callback_disable);
    	}
    	// Iniciamos el timer y mostramos un mensaje de traza si falla y estamos en mdo DEBUG
    	ASSERT(xTimerStart( xTimer,10 )==pdPASS);
    	log(INFO_LOG_LEVEL,"Se ha programado la temporizacion de los leds satisfactoriamente para dentro de %d segundos\n",ticks_count/configTICK_RATE_HZ);
    }else
    {
    	log(ERROR_LOG_LEVEL,"No se pudo programar la temporizacion de leds\n");
    }




}


