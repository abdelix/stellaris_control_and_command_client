#include "temperatura.h"

#define QUEUE_SIZE 16

#define SECUENCIADOR 3
//Variables globales

xQueueHandle  cola_temperatura;


void temp_isr(void);
static portTASK_FUNCTION(TemperaturaTask,pvParameters);

void configurar_temp(void)
{

	SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_ADC0); //Para mantenerlo habilitado en modo sleep
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);   // Habilita ADC0
	ADCSequenceDisable(ADC0_BASE, SECUENCIADOR); // Deshabilita el secuenciador 1 del ADC0 para su configuracion

	HWREG(ADC0_BASE + ADC_O_PC) = (ADC_PC_SR_250K);	// usar en lugar de SysCtlADCSpeedSet
	ADCSequenceConfigure(ADC0_BASE, SECUENCIADOR, ADC_TRIGGER_PROCESSOR, 0);
	// Configuramos los 4 conversores del secuenciador 1 para muestreo del sensor de temperatura
	//ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_TS);
	//ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_TS);
	//ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_TS);
	// El conversor 4 es el último, y  se genera un aviso de interrupción
	//ADCSequenceStepConfigure(ADC0_BASE, 1, 3, ADC_CTL_TS | ADC_CTL_IE | ADC_CTL_END);
	ADCSequenceStepConfigure(ADC0_BASE, SECUENCIADOR, 0, ADC_CTL_TS | ADC_CTL_IE | ADC_CTL_END);
	ADCHardwareOversampleConfigure(ADC0_BASE,64);

	IntPrioritySet(INT_ADC0SS3,configMAX_SYSCALL_INTERRUPT_PRIORITY); //Cuentan los tres bits mas significativos


	//IntRegister(INT_ADC0SS3,temp_isr);

	ADCIntRegister(ADC0_BASE, SECUENCIADOR,temp_isr);

	ADCIntEnable(ADC0_BASE,SECUENCIADOR); //Faltaba<
	//IntEnable(INT_ADC0SS3);
	// Tras configurar el secuenciador, se vuelve a habilitar
		ADCSequenceEnable(ADC0_BASE, SECUENCIADOR);
	cola_temperatura=xQueueCreate(QUEUE_SIZE,sizeof(PARAM_COMANDO_TEMP_RESPUESTA));
	    if(cola_temperatura==NULL)
	    {
	    	// La cola no se ha creado correctamente
	    	while(1);
	    }

	    //
		// Create la tarea que gestiona  la temperatura
		//
		if((xTaskCreate(TemperaturaTask, (signed portCHAR *)"Temperatura", 512,NULL,tskIDLE_PRIORITY + 1, NULL) != pdTRUE))
		{
			while(1)
			{
			}
		}

}

void iniciar_conv_temp(){
	// Dispara una nueva secuencia de conversiones
	ADCProcessorTrigger(ADC0_BASE, SECUENCIADOR);
}

void temp_isr(void){
	uint32_t ui32TempAvg;
	float TempValueC;
	uint32_t ui32ADC0Value=33;
	PARAM_COMANDO_TEMP_RESPUESTA param;
	signed portBASE_TYPE higherPriorityTaskWoken=pdFALSE;	//Hay que inicializarlo a False!!

	ADCIntClear(ADC0_BASE, SECUENCIADOR); // Limpia el flag de interrupción del ADC
	//Leemos los datos del secuenciador del array
    log(INFO_LOG_LEVEL,"El adc a devuelto >%d< muestras\n",ADCSequenceDataGet(ADC0_BASE,SECUENCIADOR , &ui32ADC0Value));


	// Calculamos la temperatura media como la media de las muestras de los 4 conversores
	ui32TempAvg = (ui32ADC0Value );//+ ui32ADC0Value[1] + ui32ADC0Value[2] + ui32ADC0Value[3] + 2)/4;

	log(INFO_LOG_LEVEL,"La muestra tomada es: %d\n",ui32TempAvg);

	// Y lo convertimos a grados centigrados y Farenheit, usando la formula indicada en el Data Sheet
	TempValueC = (1475.0 - ((2475.0 *(float)ui32TempAvg)) / 4096.0)/10.0;

	param.temp=TempValueC;

	xQueueSendFromISR(cola_temperatura,&param,&higherPriorityTaskWoken);

	    //El tercer parametro es un flag que determina si se ha desbloqueado
	    //a otra tarea como consecuencia de la operacion

	    //Si se desbloquea dicha tarea, hay que usar vPortYieldFromISR para despertar la tarea
		// Ahora hay que comprobar si hay que hacer el cambio de contexto

	if (pdTRUE==higherPriorityTaskWoken) vPortYieldFromISR();
}

static portTASK_FUNCTION(TemperaturaTask,pvParameters)
{
	PARAM_COMANDO_TEMP_RESPUESTA param;

	// Lo declaramos como static para prevenir que se guarde en la pila
	static unsigned char frame[MAX_FRAME_SIZE];
    int32_t numdatos;
    //
    // Loop forever.
    //
    while(1)
    {
        // Nos bloqueamos hasta que haya otro mensaje
    	//
    	xQueueReceive(cola_temperatura,&param,portMAX_DELAY);
    	// Leemos el mensaje  y enviamos un paquete convenientemente formateado
    	//

    	log(INFO_LOG_LEVEL,"Se ha recibido muestra de temperatura: %d \n",(int)param.temp);

    	numdatos=create_frame(frame,COMANDO_TEMP_RESPUESTA,(void *)(&param),sizeof(param),MAX_FRAME_SIZE);
		if (numdatos>=0)
		{
			send_frame(frame,numdatos);
		}
    }
}
