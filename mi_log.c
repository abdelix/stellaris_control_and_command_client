/*
 * log.c
 *
 *  Created on: 19/04/2014
 *      Author: Abdelfetah Hadij
 */
#include "mi_log.h"
#define MAGIC_NUMBER 0xBABADEDE
static char LOG_LEVEL_STR[][8]={"","ERROR","WARNING","INFO"};
// Nivel de log actual
static uint8_t current_log_level=SHOW_ALL_LOGS ; // por defecto muestra todos los mensajes
static uint8_t log_to_EE=0;//por defecto no guardamos los logs a la eeprom

/*
 *
 *
 */

char buf[128];
/*
 * Numero de secuencia del mensaje
 *
 */

static int seq=0;
/*
 * Set the current log level :
 *  Possible values:
 *  #define SHOW_ALL_LOGS 3
 *  #define INFO_LOG_LEVEL 3
 *  #define WARNING_LOG_LEVEL 2
 *  #define ERROR_LOG_LEVEL 1
 *  #define NO_LOG 0
 *
 */
void set_log_level(uint8_t log_level){
	current_log_level= log_level;
}

/*  void log(uint8_t log_level,char * msg,...)
 *
 *  log_level: minimo nivel de eror a mostrar este y todos los superiores se muestran
 *  msg,...  : mensaje a mostrar formateado al estilo printf
 *
 *
 */
void log(uint8_t log_level,char * msg,...)
{


	// si est�mos en u nivel de loging igual o menor lo mostramos si no no
    if(log_level<=current_log_level)
    {
    	UARTprintf("[%d] : [%s] :",seq++,LOG_LEVEL_STR[log_level]);
    	if(log_to_EE){
    			usnprintf(buf,"[%d] : [%s]:   ",seq++,LOG_LEVEL_STR[log_level]);
    			write_to_eeprom(buf);
    	}

    	/*
    	 *  copyPaste de la implementacion de  UARTPrintf y usprintf
    	 *
    	 */
    	va_list vaArgP;

		//
		// Start the varargs processing.
		//
		va_start(vaArgP, msg);

		if(log_to_EE){
			uvsnprintf(buf, 0xffff, msg, vaArgP);
		}

		UARTvprintf(msg, vaArgP);

		//
		// We're finished with the varargs now.
		//
		va_end(vaArgP);

		if(log_to_EE){
		     write_to_eeprom(buf);
		}

    }
}

// Funcion llamada por ASSERT(expr)
void __error__(char *pcFilename, uint32_t ui32Line)
{

    log(ERROR_LOG_LEVEL,"%s:%d ,  ASSERT(expr)!=TRUE en ",pcFilename,ui32Line);

}



void enviar_comando_nack(unsigned codigo_error){
	PARAM_COMANDO_NACK parametro;
	unsigned char frame[MAX_FRAME_SIZE];	//Ojo, esto hace que esta tarea necesite bastante pila
	int numdatos;

	parametro.error_code= codigo_error;

	numdatos=create_frame(frame,COMANDO_NACK,&parametro,sizeof(parametro),MAX_FRAME_SIZE);
	if (numdatos>=0)
	{
	    send_frame(frame,numdatos);
	}
}
/*
 * get the current logging level
 */

uint8_t get_log_level( )
{
 return current_log_level;
}

char* get_log_level_str( )
{
 return LOG_LEVEL_STR[current_log_level];
}

void enable_log_to_eeprom()
{
	log_to_EE=1;
}
void disable_log_to_eeprom()
{
	log_to_EE=0;
}

void init_log_to_eeprom()
{
	// inicializamos la eprom con la longitud 8 y  un numero magico
	uint32_t longitud_magic[2]={sizeof(uint32_t)*2, MAGIC_NUMBER};
    uint32_t magic;

    SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
    SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_EEPROM0);

    EEPROMInit();

    EEPROMRead(&magic,4,sizeof(uint32_t));

    // si esta en la eeprom el numero magico es que ya hay un archivo de log
	if(magic!=MAGIC_NUMBER)
	{
		EEPROMProgram(&longitud_magic,0,sizeof(uint32_t));
	}

	else {
		UARTprintf("Habia un archivo de log anterior reusando sobreescribirlo\n Procedo a escribir a continuacion\n");
        write_to_eeprom("######:\n");
	}


}

/*
 *
 *
 *
 * */



/*
 *
 * Escribe una cadena acabada en null a la eprom truncandola al multiplo de 4 mas proximo
 *
 */
void write_to_eeprom(char *data){

	uint32_t longitud_anterior,longitud_datos,longitud_posterior;
	uint32_t posicion= 0x000;

	EEPROMRead(&longitud_anterior,0,sizeof(uint32_t));

	posicion=longitud_anterior;

	longitud_datos=strlen(data)-strlen(data)%4;// truncamos a una longitud menor y multiplo de 4

    data[longitud_datos-1]='\n';

	// Program some data into the EEPROM at address posicion
	if((posicion+longitud_datos)< EEPROMSizeGet()-1){
		EEPROMProgram(data, posicion,longitud_datos );
		longitud_posterior=longitud_anterior+longitud_datos;
		EEPROMProgram(&longitud_posterior,0,sizeof(uint32_t));
	}
	else {
		log(ERROR_LOG_LEVEL,"EEPROM LLENA -> LA DESACTIVAMOS \n");
	}
}

void mostrar_eeprom(){

	uint32_t longitud,datos;
	char buf[5]={0,0,0,0,0};
	uint32_t posicion;
	int i=0;

	uint32_t magic;

	EEPROMRead(&magic,4,sizeof(uint32_t));


	if(magic==MAGIC_NUMBER)
	{
		EEPROMRead(&longitud, 0x000, sizeof(longitud));


		for(i=4;i< longitud; i+=4){
			EEPROMRead(buf, i,4);
			UARTprintf("%s",buf);
		}
	}else
	{
		UARTprintf("No se encontr� ningun archivo de log en la EEPROM\n");
	}

}
