//*****************************************************************************
//
// commands.c - FreeRTOS porting example on CCS4
//
// Copyright (c) 2012 Fuel7, Inc.
//
//*****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Standard Stellaris includes */
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"

#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/sysctl.h"
#include "drivers/buttons.h"
/* Other Stellaris include */
#include "utils/cpu_usage.h"
#include "utils/cmdline.h"
#include "utils/ustdlib.h"

#include "utils/uartstdio.h"

#include "drivers/rgb.h"
// Nuestros includes
#include "mi_log.h"
#include "temperatura.h"
#include "reloj.h"
// ==============================================================================
// The CPU usage in percent, in 16.16 fixed point format.
// ==============================================================================
extern unsigned long g_ulCPUUsage;


// ==============================================================================
// Implementa el comando cpu que muestra el uso de CPU
// ==============================================================================
int Cmd_cpu(int argc, char *argv[])
{
    //
    // Print some header text.
    //
    UARTprintf("ARM Cortex-M4F %u MHz - ",SysCtlClockGet() / 1000000);
    UARTprintf("%2u%% de uso\n", (g_ulCPUUsage+32768) >> 16);

    // Return success.
    return(0);
}

// ==============================================================================
// Implementa el comando free, que muestra cu�nta memoria "heap" le queda al FreeRTOS
// ==============================================================================
int Cmd_free(int argc, char *argv[])
{
    //
    // Print some header text.
    //
    UARTprintf("%d bytes libres\n", xPortGetFreeHeapSize());

    // Return success.
    return(0);
}

// ==============================================================================
// Implementa el comando task. S�lo es posible si la opci�n configUSE_TRACE_FACILITY de FreeRTOS est� habilitada
// ==============================================================================
#if ( configUSE_TRACE_FACILITY == 1 )

extern char *__stack;
int Cmd_tasks(int argc, char *argv[])
{
	signed char*	pBuffer;
	unsigned char*	pStack;
	portBASE_TYPE	x;
	
	pBuffer = pvPortMalloc(1024);
	vTaskList(pBuffer);
	UARTprintf("\t\t\t\tUnused\nTaskName\tStatus\tPri\tStack\tTask ID\n");
	UARTprintf("=======================================================\n");
	UARTprintf("%s", pBuffer);
	
	// Calculate kernel stack usage
	x = 0;
	pStack = (unsigned char *) &__stack;
	while (*pStack++ == 0xA5)
	{
		x++;
	}
	usprintf((char *) pBuffer, "%%%us", configMAX_TASK_NAME_LEN);
	usprintf((char *) &pBuffer[10], (const char *) pBuffer, "kernel");
	UARTprintf("%s\t\tR\t%u\t%u\t-\n", &pBuffer[10], configKERNEL_INTERRUPT_PRIORITY,
		x / sizeof(portBASE_TYPE));
	vPortFree(pBuffer);
	return 0;
}

#endif /* configUSE_TRACE_FACILITY */

// ==============================================================================
// Implementa el comando help
// ==============================================================================
int Cmd_help(int argc, char *argv[])
{
    tCmdLineEntry *pEntry;

    //
    // Print some header text.
    //
    UARTprintf("Comandos disponibles\n");
    UARTprintf("------------------\n");

    //
    // Point at the beginning of the command table.
    //
    pEntry = &g_psCmdTable[0];

    //
    // Enter a loop to read each entry from the command table.  The end of the
    // table has been reached when the command name is NULL.
    //
    while(pEntry->pcCmd)
    {
        //
        // Print the command name and the brief description.
        //
        UARTprintf("%s%s\n", pEntry->pcCmd, pEntry->pcHelp);

        //
        // Advance to the next entry in the table.
        //
        pEntry++;
    }

    //
    // Return success.
    //
    return(0);
}

// ==============================================================================
// Implementa el comando "LED"
// ==============================================================================
int Cmd_led(int argc, char *argv[])
{
	if (argc == 1)
	{
		//Si los par�metros no son suficientes, muestro la ayuda
		UARTprintf(" LED [on|off]\n");
	}
	else
	{
		//seconds = ustrtoul(argv[1], NULL, 10);

		if (0==strncmp( argv[1], "on",2))
		{
			UARTprintf("Activo los LED\n");
			//Esto era antes: (GPIO_PORTF_BASE, GPIO_PIN_3,GPIO_PIN_3);
			RGBEnable();
		}
		else if (0==strncmp( argv[1], "off",3))
		{
			UARTprintf("Desactivo los LED\n");
			RGBDisable();
			//Esto era antes: GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3,0);
		}
		else
		{
			//Si el parametro no es correcto, muestro la ayuda
			UARTprintf(" LED [on|off]\n");
		}

	}
	
    
    return 0;
}




int Cmd_buttons(int argc, char *argv[])
{
    //
    // Print some header text.
    //
    uint8_t ui8Changed,ui8Buttons;


    log(INFO_LOG_LEVEL,"Comprobando el estado de los botones...\n");

    ASSERT(1==0);

	ButtonsPoll(&ui8Changed,&ui8Buttons);

    UARTprintf("Estado Botones : \n Bot�n izquierdo : %s \n Bot�n derecho : %s  \n",
    		ui8Buttons&(LEFT_BUTTON)?"si":"no" ,(ui8Buttons&RIGHT_BUTTON)?"si":"no" );

    // Return success.
    return(0);
}


int Cmd_log(int argc, char *argv[])
{
   if(argc<2){
	   UARTprintf("Pocos argumentos: \n log [ALL/INFO/WARNING/ERROR/NOLOG]\n");
	   UARTprintf(" El nivel actual de logging es : %s \n",get_log_level_str());

   } else{
	   switch(tolower(argv[1][0])){
	   	case 'a':{
	   		set_log_level(SHOW_ALL_LOGS);
	   		UARTprintf("Log activado y puesto en nivel info/all \n");
	   	}
	   	break;
	   	case 'i':{
	   		set_log_level(INFO_LOG_LEVEL);
	   		UARTprintf("Log activado y puesto en nivel info/all \n");
	   	}
	   	break;
	   	case 'w':{
	   		set_log_level(WARNING_LOG_LEVEL);
	   		UARTprintf("Log activado y puesto en nivel warning\n");
	   	}
	   	break;
	   	case 'e':{
	   		set_log_level(ERROR_LOG_LEVEL);
	   		UARTprintf("Log activado y puesto en nivel error\n");
	   	}
	    break;
	   	case 'n':{
	   		set_log_level(NO_LOG);
	   	    UARTprintf("Log desactivado\n");
	   	}
	    break;
	   }
   }
    return(0);
}



void Cmd_eeprom_log(int argc, char *argv[])
{
	if(argc<2)
	{
		UARTprintf("eeprom_log [e]nable/[d]isable\n");
	}else
	{
         if(argv[1][0]=='e')
         {
        	 log(INFO_LOG_LEVEL,"Activando el log a la eeprom\n");
        	 enable_log_to_eeprom();
         }else if(argv[1][0]=='d') {
        	 log(INFO_LOG_LEVEL,"Desactivando el log a la eeprom\n");
        	 disable_log_to_eeprom();
         }
         else {
        	 UARTprintf("Pramaetro invalido\n");
         }
	}
}


void Cmd_eeprom_show(int argc, char *argv[])
{
	mostrar_eeprom();
}

int Cmd_time(int argc, char *argv[])
{
	struct tm date; //Estructura de tiempo
	ulocaltime(get_time(),&date);
	UARTprintf("El tiempo del sistema es: %d:%d:%d  %d/%d/%d\n",date.tm_hour,date.tm_min,date.tm_sec, date.tm_mday,date.tm_mon+1,date.tm_year+1900);

    // Return success.
    return(0);
}

int Cmd_nack_test(int argc, char *argv[])
{
	enviar_comando_nack(909769);

    // Return success.
    return(0);
}

int Cmd_temp(int argc, char *argv[])
{
     iniciar_conv_temp();
    // Return success.
    return(0);
}

// ==============================================================================
// Tabla con los comandos y su descripci�n. Si quiero a�adir alguno, debo hacerlo aqu�
// ==============================================================================
tCmdLineEntry g_psCmdTable[] =
{
    { "help",     Cmd_help,      "     : Lista de comandos" },
    { "?",        Cmd_help,      "        : lo mismo que help" },
    { "cpu",      Cmd_cpu,       "      : Muestra el uso de  CPU " },
    { "led",  	  Cmd_led,       "  : Apaga y enciende el led verde" },
    { "free",     Cmd_free,      "     : Muestra la memoria libre" },
    { "buttons", Cmd_buttons,    "     : Muestra el estado de los botones" },
    { "eeprom_log",     Cmd_eeprom_log,  "    :Activa y desactiva la escritura de  trazas en la eeprom " },
    { "eeprom_show",     Cmd_eeprom_show,  "    :Muestra los logs en la eeprom " },

    { "log",     Cmd_log,      "     :Activa y desactiva las trazas" },
    { "time",     Cmd_time,      "     :Muestra la hora del sistema" },
    { "nack",     Cmd_nack_test,      "     :Prueba el nack" },
    { "temp",     Cmd_temp,      "     :Fuerza la lectura de  la temperatura" },
#if ( configUSE_TRACE_FACILITY == 1 )
	{ "tasks",    Cmd_tasks,     "    : Muestra las estadisticas de las tareas" },
#endif
    { 0, 0, 0 }
};


// ==============================================================================
// Tarea UARTTask.  Espera la llegada de comandos por el puerto serie y los ejecuta al recibirlos...
// ==============================================================================
extern xSemaphoreHandle g_xRxLineSemaphore;
void UARTStdioIntHandler(void);

void vUARTTask( void *pvParameters )
{
    char    cCmdBuf[64];
    int     nStatus;
	

	UARTprintf("\n\n FreeRTOS %s \n",
		tskKERNEL_VERSION_NUMBER);
	UARTprintf("\n Teclee ? para ver la ayuda \n");
	UARTprintf("> ");

	/* Loop forever */
    while (1)
    {
		/* Wait for a signal indicating we have an RX line to process */
    	//!!Cambiar
 		xSemaphoreTake(g_xRxLineSemaphore, portMAX_DELAY);
    	
	    if (UARTPeek('\r') != -1)
	    {
	    	/* Read data from the UART and process the command line */
	        UARTgets(cCmdBuf, sizeof(cCmdBuf));
	        if (ustrlen(cCmdBuf) == 0)
	        {
	            UARTprintf("> ");
	            continue;
	        }
	
	        //
	        // Pass the line from the user to the command processor.  It will be
	        // parsed and valid commands executed.
	        //
	        nStatus = CmdLineProcess(cCmdBuf);
	
	        //
	        // Handle the case of bad command.
	        //
	        if(nStatus == CMDLINE_BAD_CMD)
	        {
	            UARTprintf("Comando erroneo!\n");	//No pongo acentos adrede
	        }
	
	        //
	        // Handle the case of too many arguments.
	        //
	        else if(nStatus == CMDLINE_TOO_MANY_ARGS)
	        {
	            UARTprintf("El interprete de comandos no admite tantos parametros\n");	//El m�ximo, CMDLINE_MAX_ARGS, est� definido en cmdline.c
	        }
	
	        //
	        // Otherwise the command was executed.  Print the error code if one was
	        // returned.
	        //
	        else if(nStatus != 0)
	        {
	            UARTprintf("El comando devolvio el error %d\n",nStatus);
	        }
	
	        UARTprintf("> ");
	    }
    }
}
