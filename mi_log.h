/*
 *  Libreria que contiene las funciones de traza
 *
 */



#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
//#include <assert.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Standard Stellaris includes */
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_eeprom.h"

#define DEBUG
#include "driverlib/debug.h"

/* Other Stellaris include */

#include "utils/ustdlib.h"

#include "utils/uartstdio.h"

#include <stdarg.h>
// Activamos la sentencia ASSERT(expr)
#include "driverlib/debug.h"
#include "protocol.h"



#ifndef __MI_LOG_H__
#define __MI_LOG_H__

#define SHOW_ALL_LOGS 3
#define INFO_LOG_LEVEL 3
#define WARNING_LOG_LEVEL 2
#define ERROR_LOG_LEVEL 1
#define NO_LOG 0




void log(uint8_t log_level,char * msg,...);
void set_log_level(uint8_t log_level);
uint8_t get_log_level( );
void __error__(char *pcFilename, uint32_t ui32Line);
void enviar_comando_nack(unsigned codigo_error);
uint8_t get_log_level( );
char* get_log_level_str( );



void write_to_eeprom(char *data);
/*Muestra el contenido de la eeprom*/
void mostrar_eeprom();
/*Inicia archivo de log en la eeprom*/
void init_log_to_eeprom();
/* Habilita el log a la eprom*/
void enable_log_to_eeprom();
/*Deshabolita el log a la eeprom*/
void disable_log_to_eeprom();


#endif //__MI_LOG_H__
