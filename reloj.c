/*
 * reloj.c
 *
 *  Created on: 06/05/2014
 *      Author: Rocio
 */

#include "reloj.h"

static uint32_t wakeup_time=0; //Instante en  el que se ha encendido el sistema

// Tiempo que ha pasado desde el encendido en segundos
uint32_t time_awake()
{
	return xTaskGetTickCount()/configTICK_RATE_HZ;
}


//Funcion que devuelve la hora del sistema
uint32_t get_time(void){

	uint32_t hora_sist;
	hora_sist = wakeup_time + time_awake(); //Segundos del sistema

	return hora_sist;
}


//Cambia la hora
void set_time(uint32_t new_time){

	struct tm date; //Estructura de tiempo
	wakeup_time =  new_time - time_awake();
	ulocaltime(wakeup_time,&date);
	log(INFO_LOG_LEVEL,"Se est� acualizando la hora a: %d:%d:%d  %d/%d/%d\n",date.tm_hour,date.tm_min,date.tm_sec, date.tm_mday,date.tm_mon+1,date.tm_year+1900);
}



