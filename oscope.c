/*
 * oscope.c
 *
 *  Created on: 11/05/2014
 *
 */

#include "oscope.h"

//Prototipos de funciones privadas
void configurar_timer();
void configurar_adc();
static portTASK_FUNCTION(OscopeTask,pvParameters);

//Variables globales
OscopeConfig_t config;
uint32_t num_muestras;
xQueueHandle cola_oscope;

// para debug
uint32_t n_recv=0;

void oscope_init(){
	config.Fs= 10000;
	config.R= 115200;


	configurar_adc();
	configurar_timer();

   // start_oscope();

	cola_oscope=xQueueCreate(64,sizeof(uint8_t));
	if(NULL==cola_oscope){
		while(1); //Si hay problemas para crear la colas
	}

	if((xTaskCreate(OscopeTask, (const portCHAR *)"Oscope", 1024 ,NULL,tskIDLE_PRIORITY + 2, NULL) != pdTRUE))
	{
		while(1)
		{
		}
	}
}

void timer_isr(){ //Tratamiento interrupcion Timer
	TimerIntClear(TIMER5_BASE,TIMER_TIMA_TIMEOUT); //Limpia el flag de interrupcion

	//habilitar la interrupcion del adc
	ADCIntEnable(ADC1_BASE,3); //Faltaba<
	IntEnable(INT_ADC1SS3);
	num_muestras=0;
}

void configurar_timer(){
	uint32_t ui32Period, period_match;

	ui32Period =(SysCtlClockGet()/config.Fs); //Para que tenga un periodo de config.FS

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5); //Habilita el periferico TIMER 5
	ROM_SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_TIMER5);
	//Configura el timer para cuenta periodica
	TimerConfigure(TIMER5_BASE, TIMER_CFG_PERIODIC);

	TimerLoadSet(TIMER5_BASE, TIMER_A, ui32Period -1);
	//IntRegister(INT_TIMER5A,timer_isr);

	//IntEnable(INT_TIMER5A); //Habilita la interrupcion del timer
	//TimerIntEnable(TIMER5_BASE, TIMER_TIMA_TIMEOUT); //Habilita dentro del modulo TIMER5, la interrupcion de fin de cuenta

	// no cambiamos la prioridad de interupcion del timer porque en la isr no usamos la api de freeRTOS
	TimerControlTrigger(TIMER5_BASE,TIMER_A, true); //Disparo del ADC



}


void adc_isr()
{
   num_muestras++;
   uint32_t muestras[8],count;
   uint8_t muestra_8bits;

   signed portBASE_TYPE higherPriorityTaskWoken=pdFALSE;	//Hay que inicializarlo a False!!

   ADCIntClear(ADC1_BASE, 3);

   count=ADCSequenceDataGet(ADC1_BASE, 3, muestras);

   muestra_8bits=(uint8_t)(muestras[0]>>4);// descartamos los 4 bits menos significativos

   if(xQueueIsQueueFullFromISR(cola_oscope))
   {
	   xQueueReset(cola_oscope);
       num_muestras=0;
   }



   xQueueSendFromISR(cola_oscope,&muestra_8bits,&higherPriorityTaskWoken);
   if (pdTRUE==higherPriorityTaskWoken) vPortYieldFromISR();


}

void configurar_adc(){

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);   // Habilita ADC1
	SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_ADC1); //Para mantenerlo habilitado en modo sleep

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_GPIOE);

	ADCSequenceDisable(ADC1_BASE, 3); // Deshabilita el secuenciador 3 del ADC1 para su configuracion

	HWREG(ADC1_BASE + ADC_O_PC) = (ADC_PC_SR_1M);	// usar en lugar de SysCtlADCSpeedSet


	ADCSequenceConfigure(ADC1_BASE, 3, ADC_TRIGGER_TIMER, 0);


	GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2);// pin2 es AIN1
	ADCSequenceStepConfigure(ADC1_BASE, 3, 0, ADC_CTL_CH1 | ADC_CTL_IE | ADC_CTL_END);

	ADCHardwareOversampleConfigure(ADC1_BASE,64); //Promediado hardware de 4 muestras
	IntPrioritySet(INT_ADC1SS3,configMAX_SYSCALL_INTERRUPT_PRIORITY +(1<<5)); //Cuentan los tres bits mas significativos



	// Tras configurar el secuenciador, se vuelve a habilitar
	ADCSequenceEnable(ADC1_BASE, 3);

	IntRegister(INT_ADC1SS3,adc_isr);

	ADCIntEnable(ADC1_BASE,3); //Faltaba<
    IntEnable(INT_ADC1SS3);

}


void oscope_config_set_Fs(uint32_t fs){



	if((config.Fs*10)> config.R)
	{
		 // es probable que se produzca un cuello de botella -> avisamos al pc
         enviar_comando_nack(142);
	}

	else
	{
		stop_oscope();
		config.Fs=fs;

		configurar_timer();
	}


}


/*
 *
 * Inicia el osciloscopio
 */

void start_oscope(){

		TimerEnable(TIMER5_BASE, TIMER_A); //Activa el TIMER5A
}
/*
 * Deteiene el osciloscopio
 *
 */
void stop_oscope(){
	TimerDisable(TIMER5_BASE, TIMER_A); //Activa el TIMER5A
}



static portTASK_FUNCTION(OscopeTask,pvParameters)
{
	PARAM_COMANDO_OSCOPE_SAMPLES param;
	int32_t numdatos;


    int i,j;
	// Lo declaramos como static para prevenir que se guarde en la pila
	static unsigned char frame[MAX_FRAME_SIZE];


	while(1)
	{




			for(i=0;i<MAX_DATA_SIZE;i++){
			 xQueueReceive(cola_oscope,&param.samples[i],portMAX_DELAY);
			}



			numdatos=create_frame(frame,COMANDO_OSCOPE_SAMPLES,(void *)(&param),sizeof(param),MAX_FRAME_SIZE);

			if (numdatos>=0)
			{

				send_frame(frame,numdatos);

				log(INFO_LOG_LEVEL,"Se ha enviado un grupo de muestras \n");
				num_muestras-=31;
			}



	}

}

