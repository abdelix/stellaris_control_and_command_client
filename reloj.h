/*
 * reloj.h
 *
 *  Created on: 06/05/2014
 *      Author: Rocio
 */

#ifndef RELOJ_H_
#define RELOJ_H_

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
//#include <assert.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Standard Stellaris includes */
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"

/* Other Stellaris include */

#include "utils/ustdlib.h"
#include "utils/uartstdio.h"
#include "mi_log.h"

// Tiempo que ha pasado desde el encendido en segundos
uint32_t time_awake();

//Funcion que devuelve la hora del sistema
uint32_t get_time(void);

//Cambia la hora
void set_time(uint32_t new_time);


#endif /* RELOJ_H_ */
