/*
 * botones_asincronos.c
 *
 *  Created on: 17/04/2014
 *      Author: assa
 */
// Includes necesarios


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

/* FreeRTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Standard TIVA includes */
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_adc.h"

#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "drivers/buttons.h"
#include "utils/cpu_usage.h"

// Includes nuestros

#include "protocol.h"
#include "botones_asincronos.h"

// Constantes

#define QUEUE_SIZE 16

/*
 *
 *  Variables Globales
 *
 */

// Cola de mensajes para pasar a la tarea las actualizacion del estado de los botones
xQueueHandle  cola_estado_botones;



/*
 *
 *  Prototipos de funcion
 *
 */

void buttons_isr();

static portTASK_FUNCTION(BotonesAsincronosTask,pvParameters);
/* Funcion para inicializar los perifericos
 * debe ser llamada en el main()
*/
void inicializar_botones_asincronos()
{
	// Activar la interrupcion
	// enlazar la isr
	// crear la tarea
	// ....

	/*
	 *  Tecnicamente no hace falta pues ya la llamamos en el main
	 *
	 */
    ButtonsInit();
    ROM_SysCtlPeripheralSleepEnable(GPIO_PORTF_BASE);


    ROM_GPIOIntTypeSet(GPIO_PORTF_BASE, ALL_BUTTONS,GPIO_BOTH_EDGES ); // queremos responder a los dos flancos


	// Prioridad inferior inmediata a configMAX_SYSCALL_INTERRUPT_PRIORITY
	// pues usamos la api de freertos en la interrupcion
	ROM_IntPrioritySet(INT_GPIOF,configMAX_SYSCALL_INTERRUPT_PRIORITY+ (1 << 5));


	//IntEnable(INT_GPIOF); asi est� mal !! -> no se activan las fuentes
    // asi si esta bien :

	/*
	 *  aun no activamos la interupci�n para estar concordes con la interfaz grafica :
	 *
	 */
	//GPIOIntEnable(GPIO_PORTF_BASE,ALL_BUTTONS);

	// A�adimos a la tabla la interrupcion
    GPIOIntRegister(GPIO_PORTF_BASE,buttons_isr);


    cola_estado_botones=xQueueCreate(QUEUE_SIZE,sizeof(PARAM_COMANDO_ESTADO_BOTONES_ASINCRONOS));
    if(cola_estado_botones==NULL)
    {
    		// La cola no se ha creado correctamente
    	    while(1);
    }

    //
	// Create la tarea que gestiona los leds en respuesta a la temperatura
	//
	if((xTaskCreate(BotonesAsincronosTask, (signed portCHAR *)"Async\nButtons", 512,NULL,tskIDLE_PRIORITY + 1, NULL) != pdTRUE))
		{
			while(1)
			{
			}
		}

}

/*
 * Isr para tratar los botones
 *
 */

void buttons_isr()
{



	// Leemos el estado de los botones
	signed portBASE_TYPE higherPriorityTaskWoken=pdFALSE;	//Hay que inicializarlo a False!!
	int32_t i32PinStatus=ROM_GPIOPinRead(GPIO_PORTF_BASE,ALL_BUTTONS);
	PARAM_COMANDO_ESTADO_BOTONES_ASINCRONOS param;

	// Bajamos el flag
	    GPIOIntClear(GPIO_PORTF_BASE,ALL_BUTTONS);

	// Los botones son activos a nivel bajo!!! Cuando esta pulsado vale 0, por eso lo negamos (!)
	param.botones.boton_derecho=!(i32PinStatus&RIGHT_BUTTON);
	param.botones.boton_izquierdo=!(i32PinStatus&LEFT_BUTTON);

	// encolamos

    xQueueSendFromISR(cola_estado_botones,&param,&higherPriorityTaskWoken);

    //El tercer parametro es un flag que determina si se ha desbloqueado
    //a otra tarea como consecuencia de la operacion

    //Si se desbloquea dicha tarea, hay que usar vPortYieldFromISR para despertar la tarea
	// Ahora hay que comprobar si hay que hacer el cambio de contexto

	if (pdTRUE==higherPriorityTaskWoken) vPortYieldFromISR();

}


/*
 *  Rutina para procesar el comando de activacion/desactivacion
 *  del envio asincrono de btotones
 *
 */

void  comando_botones_asincronos_configurar(PARAM_COMANDO_BOTONES_ASINCRONOS *param)
{
    // Estructura que contiene los parametros

	if(param->activar)
	{ // comprobamos si hay que activar habilitamos la interrupcion de los botones

		// Bajamos el flag por si acaso
		GPIOIntClear(GPIO_PORTF_BASE,ALL_BUTTONS);
		// Activamos la interupci�n
		GPIOIntEnable(GPIO_PORTF_BASE,ALL_BUTTONS);

	}
	else
	{ // En caso contrario desahbilitamos la interrupcion de los botones
       GPIOIntDisable(GPIO_PORTF_BASE,ALL_BUTTONS);
	}

}

/*
 *  Tarea que se encargara de enviar al pc  los mensajes de la cola
 *
 */

static portTASK_FUNCTION(BotonesAsincronosTask,pvParameters)
{
	PARAM_COMANDO_ESTADO_BOTONES_ASINCRONOS param;

	// Lo declaramos como static para prevenir que se guarde en la pila
	static unsigned char frame[MAX_FRAME_SIZE];
    int32_t numdatos;
    //
    // Loop forever.
    //
    while(1)
    {
        // Nos bloqueamos hasta que haya otro mensaje
    	//
    	xQueueReceive(cola_estado_botones,&param,portMAX_DELAY);
    	// Leemos el mensaje  y enviamos un paquete convenientemente formateado
    	//
    	numdatos=create_frame(frame,COMANDO_ESTADO_BOTONES_ASINCRONOS,(void *)(&param),sizeof(param),MAX_FRAME_SIZE);
		if (numdatos>=0)
		{
			send_frame(frame,numdatos);
		}
    }
}
