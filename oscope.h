/*
 * oscope.h
 *
 *  Created on: 11/05/2014
 *
 *     El muestreo se har� en tiempo real .
 *     Se usara un metodo de conversion y transferencia por rafagas :
 *
 *  ====================================
	o como funciona:
		1. se toman muestras de la se�al r�pidamente hasta llenar
		una buffer

		2. se detiene el proceso de muestreo mientras se procesan
		(lentamente) los datos

		3. se vuelve a repetir el proceso


	o cuello de botella: velocidad del microprocesador, velocidad
	transmisi�n por el bus serie/USB.

	o cuando y porque se usa:
	se usa cuando el cuello de botella NO est� en el ADC sino
	en otra etapa posterior (velocidad del microprocesador,
	transmisi�n placa-PC, etc)
	se usa para medir se�ales peri�dicas r�pidas
	se puede usar tambi�n para medir se�ales muy lentas
	(tanto peri�dicas como no peri�dicas)
	no se puede usar para medir se�ales de frecuencia media

 *
 *
 */

#ifndef OSCOPE_H_
#define OSCOPE_H_
#include<stdbool.h>
#include<stdint.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "drivers/buttons.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "utils/cpu_usage.h"
#include "timers.h"
#include "inc/hw_adc.h"

#include "drivers/rgb.h"
#include "usb_dev_serial.h"
#include "protocol.h"
// includes nuestros
#include "mi_log.h"





typedef struct {
	unsigned Fs; 				//Frecuencia de sampling
    unsigned R;


}OscopeConfig_t;


/*
 *
 * Inicializa el adc con los parametros deseados
 */
void oscope_init();

/*
 * Configura  la frecuencia de muestreo del osciloscopio
 */
void oscope_config_set_Fs(uint32_t fs);



/*
 * Inicia la toma de muestras
 *
 */
void start_oscope();
/*
 * Detiene el osciloscopio
 */
void stop_oscope();
#endif /* OSCOPE_H_ */
