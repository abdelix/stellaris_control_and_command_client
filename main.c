//*****************************************************************************
//
// freertos_demo.c - Simple FreeRTOS example.
//
// Copyright (c) 2012 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 9453 of the EK-LM4F120XL Firmware Package.
//
//*****************************************************************************

#include<stdbool.h>
#include<stdint.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "drivers/buttons.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "utils/cpu_usage.h"
#include "timers.h"

#include "drivers/rgb.h"
#include "usb_dev_serial.h"
#include "protocol.h"
// includes nuestros
#include "mi_log.h"
#include "oscope.h"
#include "temperatura.h"
#include "reloj.h"
#include "botones_asincronos.h"


#define LED1TASKPRIO 1
#define LED1TASKSTACKSIZE 128


//Globales

uint32_t g_ulCPUUsage;
uint32_t g_ulSystemClock;


extern void vUARTTask( void *pvParameters );


//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************4
// Esta definida en log.c
//#ifdef DEBUG
//void
//__error__(char *pcFilename, unsigned long ulLine)
//{
//}
//
//#endif

//*****************************************************************************
//
// Aqui incluimos los "ganchos" a los diferentes eventos del FreeRTOS
//
//*****************************************************************************

//Esto es lo que se ejecuta cuando el sistema detecta un desbordamiento de pila
//
void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName)
{
    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}

//Esto se ejecuta cada Tick del sistema. LLeva la estadistica de uso de la CPU (tiempo que la CPU ha estado funcionando)
void vApplicationTickHook( void )
{
	static unsigned char count = 0;

	if (++count == 10)
	{
    	g_ulCPUUsage = CPUUsageTick();
    	count = 0;
	}
	//return;
}

//Esto se ejecuta cada vez que entra a funcionar la tarea Idle
void vApplicationIdleHook (void)
{
    SysCtlSleep();
}


//Esto se ejecuta cada vez que entra a funcionar la tarea Idle
void vApplicationMallocFailedHook (void)
{
	while(1);
}


//*****************************************************************************
//
// A continuacion van las tareas...
//
//*****************************************************************************

static portTASK_FUNCTION(LEDTask,pvParameters)
{

    int estado_led=0;

    //
    // Bucle infinito, las tareas en FreeRTOS no pueden "acabar", deben "matarse" con la funcion xTaskDelete().
    //
    while(1)
    {
       estado_led=!estado_led;

        if (estado_led)
        {
        	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 , GPIO_PIN_1);
        	vTaskDelay(0.1*configTICK_RATE_HZ);        //Espera del RTOS (eficiente, no gasta CPU)
        	        								 //Esta espera es de unos 100ms aproximadamente.
        }
        else
        {
        	GPIOPinWrite(GPIO_PORTF_BASE,  GPIO_PIN_1,0);
        	vTaskDelay(2*configTICK_RATE_HZ);        //Espera del RTOS (eficiente, no gasta CPU)
        	                	        		   //Esta espera es de unos 2s aproximadamente.
        }
    }
}

//Esta tarea esta definida en el fichero command.c, es la que se encarga de procesar los comandos del interprete.
//Aqui solo la declaramos para poderla crear en la funcion main.
extern void vUARTTask( void *pvParameters );


uint32_t g_ulColors[3] = { 0x0000, 0x0000, 0x0000 };
static portTASK_FUNCTION( CommandProcessingTask, pvParameters ){


  unsigned char frame[MAX_FRAME_SIZE];	//Ojo, esto hace que esta tarea necesite bastante pila
  int numdatos;
  int errors=0;
  unsigned char command;



  /* The parameters are not used. */
  ( void ) pvParameters;

  //Inicializa los LEDs...
  RGBInit(1);

  //TIVA Nuevo (�Cambios RGB?)
  SysCtlPeripheralSleepEnable(GREEN_TIMER_PERIPH);
  SysCtlPeripheralSleepEnable(BLUE_TIMER_PERIPH);
  SysCtlPeripheralSleepEnable(RED_TIMER_PERIPH);	//Redundante porque son el mismo, pero bueno...


  for(;;)
  {
    numdatos=receive_frame(frame,MAX_FRAME_SIZE);

    if (numdatos>0)
    {	//Si no hay error, proceso la trama que ha llegado.
      numdatos=destuff_and_check_checksum(frame,numdatos);
      if (numdatos<0)
      {
        //Error de checksum, ignorar el paquete
        errors++;
        //Hacer algo mas?
      }
      else
      {
        //El paquete esta bien, luego procedo a tratarlo.
        command=decode_command_type(frame,0);

        switch(command)
        {
        case COMANDO_PING :
          //A un comando de ping se responde con el propio comando
          numdatos=create_frame(frame,command,0,0,MAX_FRAME_SIZE);
          if (numdatos>=0)
          {
            send_frame(frame,numdatos);
          }
          break;
        case COMANDO_LEDS:
          {
            PARAM_COMANDO_LEDS parametro;

            if (check_command_param_size(numdatos,sizeof(parametro)))
            {
              extract_packet_command_param(frame,sizeof(parametro),&parametro);
              //g_ulColors[0]=parametro.red*0x100 ;// maximo FFFF
              g_ulColors[0]=((uint32_t )parametro.red )<<8 ; //Movemos 8 bits a la izda
              g_ulColors[1]=((uint32_t )parametro.green )<<8;
              g_ulColors[2]= ((uint32_t )parametro.blue )<<8;

              RGBColorSet(g_ulColors);
            }
            else
            {
              //Error en tamanio, ignorar la orden
              errors++;
              //Hacer algo mas?
            }
          }
          break;
        case COMANDO_BRILLO:
        {
        	PARAM_COMANDO_BRILLO parametro_brillo;
        	if (check_command_param_size(numdatos,sizeof(parametro_brillo))){
        		extract_packet_command_param(frame,sizeof(parametro_brillo),&parametro_brillo);
        		RGBIntensitySet(parametro_brillo.intensity);
        	} else {
              //Error en tamanio, ignorar la orden
              errors++;
              //Hacer algo mas?
            }
        }
        break;
        case COMANDO_ESTADO_BOTONES_PREGUNTA:
        {
        	static uint8_t ui8Buttons, ui8Changed;

        	PARAM_COMANDO_ESTADO_BOTONES_RESPUESTA param_estado_bot;

        	ButtonsPoll(&ui8Changed,&ui8Buttons);

        	// Las etiquetas LEFT_BUTTON, RIGHT_BUTTON, y ALL_BUTTONS estan definidas en /drivers/buttons.h
        	param_estado_bot.botones.boton_derecho=!((RIGHT_BUTTON & ui8Buttons)==0);
        	param_estado_bot.botones.boton_izquierdo=!((LEFT_BUTTON & ui8Buttons)==0);

        	numdatos=create_frame(frame,COMANDO_ESTADO_BOTONES_RESPUESTA,(void *)(&param_estado_bot),sizeof(param_estado_bot),MAX_FRAME_SIZE);
        	if (numdatos>=0)
        	{
				send_frame(frame,numdatos);
			}

        }
        break;

        case COMANDO_BOTONES_ASINCRONOS:
        {
             PARAM_COMANDO_BOTONES_ASINCRONOS param;

             if (check_command_param_size(numdatos,sizeof(param))){
					extract_packet_command_param(frame,sizeof(param),&param);

					comando_botones_asincronos_configurar(&param);

				} else {
				   //Error en tamanio, ignorar la orden
				   errors++;
				   //Hacer algo mas?
				 }

        }
        break;

        case COMANDO_TEMP_PREGUNTA:
        {
        	//Disparar una nueva secuencia de conversiones
        	iniciar_conv_temp();
        }
        break;

        case COMANDO_TIME:
        {
        	PARAM_COMANDO_TIME param;
        	if (check_command_param_size(numdatos,sizeof(param))){
        		extract_packet_command_param(frame,sizeof(param),&param);

        		set_time(param.time);
        		log(INFO_LOG_LEVEL,"Hora actualizada correctamente\n");


        	} else {
			   //Error en tamanio, ignorar la orden
			   errors++;
			   //Hacer algo mas?
			   log(ERROR_LOG_LEVEL,"Error en comando time\n");
			 }
        }
        break;

        case COMANDO_TIME_REQUEST:
		{
			PARAM_COMANDO_TIME parametro;
			parametro.time=get_time();
			numdatos=create_frame(frame,COMANDO_TIME,&parametro,sizeof(parametro),MAX_FRAME_SIZE);
			if (numdatos>=0)
			{
			  send_frame(frame,numdatos);
			}
		}
		break;

        case COMANDO_PROGRAMAR_LEDS:
		{
			PARAM_COMANDO_PROGRAMAR_LEDS param;


			if (check_command_param_size(numdatos,sizeof(param))){
				extract_packet_command_param(frame,sizeof(param),&param);

				temporizar_leds(&param);



			} else {
			   //Error en tama�o, ignorar la orden
			   errors++;
			   //Hacer algo mas?
			   log(ERROR_LOG_LEVEL,"Error en comando programar hora\n");
			 }
		}

        case COMANDO_START_OSCOPE:
        {
                  start_oscope();
        }
        break;

        case COMANDO_STOP_OSCOPE:
		{
				  stop_oscope();
		}
		break;


        case COMANDO_CONFIG_OSCOPE:
        		{
        			     //paramos el osciloscopio si estaba
        				  stop_oscope();

        				  PARAM_COMANDO_CONFIG_OSCOPE param;


        				  if (check_command_param_size(numdatos,sizeof(param))){
        				  				extract_packet_command_param(frame,sizeof(param),&param);

        				  		oscope_config_set_Fs(param.Fs);
        				  }



        		}
        		break;
        default:
          {
            PARAM_COMANDO_NO_IMPLEMENTADO parametro;
            parametro.command=command;
            //El comando esta bien pero no esta implementado
            numdatos=create_frame(frame,COMANDO_NO_IMPLEMENTADO,&parametro,sizeof(parametro),MAX_FRAME_SIZE);
            if (numdatos>=0)
            {
              send_frame(frame,numdatos);
            }
            break;
          }
        }
      }
    }
  }
}



//*****************************************************************************
//
// Funcion main(), Inicializa los perifericos, crea las tareas, etc... y arranca el bucle del sistema
//
//*****************************************************************************
int main(void)
{

    //
    // Set the clocking to run at 50 MHz from the PLL.
    //
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);	//Ponermos el reloj principal a 50 MHz (200 Mhz del Pll dividido por 4)


    // Get the system clock speed.
    g_ulSystemClock = SysCtlClockGet();


    //Habilita el clock gating de los perifericos durante el bajo consumo
    ROM_SysCtlPeripheralClockGating(true);

    // Inicializa el subsistema de medida del uso de CPU (mide el tiempo que la CPU no esta dormida)
    // Para eso utiliza un timer, que aqui hemos puesto que sea el TIMER3 (ultimo parametro que se pasa a la funcion)
    // (y por tanto este no se deberia utilizar para otra cosa).
    // El timer 0 y 1 se utilizan para los LEDS con la libreria RGB
    CPUUsageInit(g_ulSystemClock, configTICK_RATE_HZ/10, 3);

    //
    // Inicializa la UARTy la configura a 115.200 bps, 8-N-1 .
    //se usa para mandar y recibir mensajes y comandos por el puerto serie
    // Mediante un programa terminal como gtkterm, putty, cutecom, etc...
    //
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTStdioConfig(0,115200,SysCtlClockGet());

    ROM_SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_UART0);	//La UART tiene que seguir funcionando aunque el micro est� dormido
    ROM_SysCtlPeripheralSleepEnable(SYSCTL_PERIPH_GPIOA);	//La UART tiene que seguir funcionando aunque el micro est� dormido

    // configuramos los botones
     ButtonsInit();
    // Inicializamos los botones asincronos
     inicializar_botones_asincronos();
     //Configurar temp
     configurar_temp();
     //Inicializar configuracion osciloscopio
     oscope_init();

     //iciamos el logging en el nivel mas verbose y inciamos el log a ala eeprom

     set_log_level(SHOW_ALL_LOGS);


     init_log_to_eeprom();

    //Inicializa el puerto F (LEDs) --> No hace falta si usamos la libreria RGB
//    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
//    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
//    ROM_GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, 0);	//LEDS APAGADOS



    //
    // Mensaje de bienvenida inicial.
    //
    UARTprintf("\n\nWelcome to the TIVA TM4C123GXL FreeRTOS Demo!\n");




    //
    // Crea la tarea que gestiona los comandos UART (definida en el fichero commands.c)
    //
    if((xTaskCreate(vUARTTask, (signed portCHAR *)"Uart", 512,NULL,tskIDLE_PRIORITY + 1, NULL) != pdTRUE))
        {

            while(1)
            {
            }
        }


    UsbSerialInit(32,32);	//Inicializo el  sistema USB
    //
    // Crea la tarea que gestiona los comandos USB (definidos en CommandProcessingTask)
    //
    if(xTaskCreate(CommandProcessingTask, (signed portCHAR *)"usbser",512, NULL, tskIDLE_PRIORITY + 3, NULL) != pdTRUE)
    {
        while(1);
    }

    //
    // Arranca el  scheduler.  Pasamos a ejecutar las tareas que se hayan activado.
    //
    vTaskStartScheduler();	//el RTOS habilita las interrupciones al entrar aqui, asi que no hace falta habilitarlas

    //De la funcion vTaskStartScheduler no se sale nunca... a partir de aqui pasan a ejecutarse las tareas.
    while(1)
    {
    	//Si llego aqui es que algo raro ha pasado
    }
}

